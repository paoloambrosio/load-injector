package unit

import java.io.{PrintStream, ByteArrayOutputStream, PrintWriter}
import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}
import org.bitbucket.loadinjector._

class ResultCollectorSpec extends TestKit(ActorSystem("Test"))
  with FlatSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  trait StringOutput {
    private val byteStream = new ByteArrayOutputStream
    val outputStream = new PrintStream(byteStream)
    def writtenOutput = byteStream.toString()
    def resetOutput() = byteStream.reset()
  }

  "The Result Collector" should "print script executions" in new StringOutput {
    val resultCollector = TestActorRef(new ResultCollector(outputStream))

    resultCollector ! ScriptSuccessful(10 millis, 42 millis)
    resultCollector ! ScriptFailed(56 millis, 5000 millis, new Exception("message"))

    writtenOutput must be (
      """start,elapsed,error
        |10,42,
        |56,5000,message
        |""".stripMargin)
  }

  it should "not print the header on restart" in new StringOutput {
    val resultCollector = TestActorRef(new ResultCollector(outputStream))
    val scriptResult = ScriptSuccessful(0 millis, 0 millis)
    resultCollector ! scriptResult
    resetOutput()

    resultCollector.suspend()
    resultCollector.restart(new Throwable)
    resultCollector ! scriptResult

    writtenOutput must not be empty
    writtenOutput must not include ("start,elapsed,error")
  }
}