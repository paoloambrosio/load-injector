name := """load-injector"""

scalaVersion := "2.11.1"

scalacOptions ++= Seq("-feature", "-deprecation", "-language:postfixOps")

val akkaVersion = "2.3.3"

val compileDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion
)

val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "2.1.7" % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
)

libraryDependencies := compileDependencies ++ testDependencies
